#
# display.py
# ProjetVTI
#
# Created by Jordan Chicha and Nessim Halfon on 03/10/2018.
# Copyright © 2018 Jordan Chicha and Nessim Halfon. All rights reserved
#

class Display:
    """
    Cette classe permet de generer l'affichage des donnees traitees.
    """

    def __init__(self, data_recovery):
        """
        Constructeur de la classe.
        """
        self.data_recovery = data_recovery

    def file_infos_to_string(self):
        """
        Cette methode permet de retourner une chaine de caracteres contenant les informations du fichier.
        """

        chain = "*********************************************************************\n"
        chain += "*                      Informations du fichier                      *\n"
        chain += "*********************************************************************\n\n"

        chain += "Taille\t\t\t:\t"
        # Transtypage de la taille du fichier en String.
        chain += str(self.data_recovery.get_size())
        chain += " Octets\n"
        chain += "Encoding\t\t:\t"
        # Transtypage de l'encodage du fichier en String.
        chain += str(self.data_recovery.get_encoding())+"\n"
        chain += "Dernière modification\t:\t"
        # Transtypage de la date de la derniere modification du fichier en String.
        chain += str(self.data_recovery.get_time())
        chain += "\n_____________________________________________________________________\n"
        return chain

    def df_infos_to_string(self):
        """
        Cette methode permet de retourner une chaine de caracteres contenant les informations de la DataFrame.
        """
        chain = "*********************************************************************\n"
        chain += "*                      Informations de la table                     *\n"
        chain += "*********************************************************************\n\n"

        chain += "Nombre de variables\t\t\t:\t"
        # Transtypage du nombre de variables en String.
        chain += str(self.data_recovery.get_var_number())+"\n"
        chain += "Nombre d'observations\t\t\t:\t"
        # Transtypage du nombre d'observations en String.
        chain += str(self.data_recovery.get_obs_number())+"\n"
        chain += "Nombre de variables qualitatives\t:\t"
        # Transtypage du nombre de variables qualitatives en String.
        chain += str(self.data_recovery.get_var_quali_number())+"\n"
        chain += "Nombre de variables quantitatives\t:\t"
        # Transtypage du nombre de variables quantitatives en String.
        chain += str(self.data_recovery.get_var_quanti_number())+"\n"
        chain += "Noms des variables\t\t\t:\t"
        # Transtypage du noms des variables en String.
        chain += str(self.data_recovery.get_var_names())
        chain += "\n_____________________________________________________________________\n"
        return chain

    def quali_infos_to_string(self):
        """
        Cette methode permet de retourner une chaine de caracteres contenant les informations des variables qualitatives.
        """
        chain = ''
        # On verifie en premier lieu si des variables qualitatives ont ete trouves.
        if self.data_recovery.get_var_quali_number() == 0:
            return chain
        chain = "*********************************************************************\n"
        chain += "*                      Variables Qualitatives                       *\n"
        chain += "*********************************************************************\n"

        # Parcours des differentes variables qualitatives.
        for value in self.data_recovery.get_infos_quali():
            chain += "\nVariable\t\t:\t"
            # Recuperation du nom de la variable.
            # Transtypage du nom de la variable en String.
            chain += str(value[0])+"\n"
            chain += "Nombre de modalités\t:\t"
            # Recuperation du nombre de modalites pour la variable concernee.
            # Transtypage du nombre de modaites pour la variable en concernee en String.
            chain += str(len(value[1]))+"\n\n"
            # Recuperation des informations de la variable qualitatives concernee.
            # Transtypage des informations de la variable qualitatives concernee en String.
            chain +=  str(value[1])
            chain += "\n_____________________________________________________________________\n"
        return chain

    def quanti_infos_to_string(self):
        """
        Cette methode permet de retourner une chaine de caracteres contenant les informations des variables quantitatives.
        """
        chain = ''
        # On verifie en premier lieu si des variables qualitatives ont ete trouves.
        if self.data_recovery.get_var_quanti_number() == 0:
            return chain
        chain = "*********************************************************************\n"
        chain += "*                      Variables Quantitatives                      *\n"
        chain += "*********************************************************************\n\n"

        # Parcours des differentes variables quantitatives.
        for value in self.data_recovery.get_infos_quanti():
            # Transtypage des differentes informations des variables quantitatives en String.
            chain += str(value)
        chain += "\n_____________________________________________________________________\n"
        return chain

    def display_all(self):
        """
        Cette methode permet l'affichage de toutes les informations.
            - Informations sur le fichier.
            - Informations sur la DataFrame.
            - Informations sur les variables qualitatives.
            - Informations sur les variables quantitatives.
        """
        print(self.file_infos_to_string())
        print(self.df_infos_to_string())
        print(self.quali_infos_to_string())
        print(self.quanti_infos_to_string())

    def get_all_infos(self):
        """
        Cette methode permet de retourner la totalite des informations sous la forme d'une chaine de caracteres.
        """
        return self.file_infos_to_string() + "\n" +\
               self.df_infos_to_string() + "\n" +\
               self.quali_infos_to_string() + "\n" +\
               self.quanti_infos_to_string()
