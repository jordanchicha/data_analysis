#
# my_bool.py
# ProjetVTI
#
# Created by Jordan Chicha and Nessim Halfon on 03/10/2018.
# Copyright © 2018 Jordan Chicha and Nessim Halfon. All rights reserved
#

import argparse

def my_bool(choice):
    """
    Cette methode permet la creation d'un type my_bool, visant a faciliter l'interation avec l'utilisateur.
    """
    # Retourne True si l'utilisateur saisie 'oui', 'o', 'yes', 'y', 'vrai', 'v', 'true', 't', '1'.
    # Acceptes aussi bien en minuscule ou en Majuscule.
    if choice.lower() in ('oui', 'o', 'yes', 'y', 'vrai', 'v', 'true', 't', '1'):
        return True
    # Retourne False si l'utilisateur saisie 'non', 'no', 'n', 'faux', 'false', 'f', '0'.
    # Acceptes aussi bien en minuscule ou en Majuscule.
    elif choice.lower() in ('non', 'no', 'n', 'faux', 'false', 'f', '0'):
        return False
    #
    else:
        raise argparse.ArgumentTypeError('\nLes valeurs acceptées pour Oui sont : (oui, o, yes, y, vrai, v, true, t, 1).\nLes valeurs acceptées pour Non sont : (non, no, n, faux, false, f, 0).')
