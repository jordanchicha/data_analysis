#
# plot.py
# ProjetVTI
#
# Created by Jordan Chicha and Nessim Halfon on 03/10/2018.
# Copyright © 2018 Jordan Chicha and Nessim Halfon. All rights reserved
#

import matplotlib.pyplot as plt

class Plot:
    """
    Cette classe permet la creation et l'enregistrement d'un plot.
    """

    def __init__(self, df):
        """
        Constructeur de la classe.
        """
        self.df = df

    def save_plot_quanti(self, names):
        """
        Cette methode permet de sauvegarder un plot d'une variable quantitatives.
        """
        for i in range(self.df.shape[0]):
            self.df.iloc[[i]].plot.bar(width=0.1)
            plt.xticks(fontsize=8, rotation=0)
            plt.yticks(fontsize=8)
            # Permet d'attribuer un noms aux etiquettes.
            plt.xlabel("Variable")
            plt.ylabel("Valeurs")
            # Permet de rajouter une grille dans le plot.
            plt.grid()
            # Permet de sauvegarder le plot dans un fichier PDF.
            plt.savefig("plot_quanti_"+names[i]+".pdf")
            # Permet de fermer le plot.
            plt.close()

    def save_plot_quali(self, var_name):
        """
        Cette methode permet de sauvegarder un plot d'une variable qualitatives.
        """
        # Permet de generer un diagramme en barres verticales.
        self.df.plot.bar(stacked=True, width=0.1)
        # Permet de regler la taille de la police des etiquettes du plot a 8.
        plt.xticks(fontsize=8, rotation=0)
        plt.yticks(fontsize=8)
        # Permet d'attribuer un nom differents pour chaque plot correspondant a une variable qualitatives.
        file_name = "plot_quali_"+var_name+".pdf"
        # Permet de rajouter une grille dans le plot.
        plt.grid()
        # Permet d'ajouter un titre au plot.
        plt.title('Variable : '+var_name)
        # Permet de sauvegarder les plots dans un fichier PDF.
        plt.savefig(file_name)
        # Permet de fermer le plot.
        plt.close()
