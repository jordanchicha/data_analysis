#
# main.py
# ProjetVTI
#
# Created by Jordan Chicha and Nessim Halfon on 03/10/2018.
# Copyright © 2018 Jordan Chicha and Nessim Halfon. All rights reserved
#

from check_args import CheckArgs
from data_recovery import DataRecovery
from display import Display
from dialog import Dialog
from save_file import SaveFile

if __name__ == '__main__':

    # Instantiation d'un objet CheckArgs, permettant la verification des differents arguments passes en parametres.
    check_args = CheckArgs()
    # Appel de la methode check_file, permettant de verifier la validite du fichier.
    check_args.check_file()
    # Recuperation du path.
    path_file = check_args.get_path_file()
    # Recuperation du choix du dialogue.
    is_dialog = check_args.get_is_dialog()
    # Recuperation du choix de la sauvegarde.
    is_save = check_args.get_is_save()

    # Instantiation d'un objet DataRecovery, permettant le traitement des donnees receuillies dans le fichier.
    data_recovery = DataRecovery(path_file)

    # Instantiation d'un objet Display, permettant la recuperation des donnees traitees.
    display = Display(data_recovery)

    # Instantiation d'un objet Dialog, permettant l'interaction avec l'utilisateur.
    dialog = Dialog(display)

    # Recuperation de toutes les informations sous forme de chaine de caracteres.
    data_to_save = display.get_all_infos()

    # Cas ou l'utilisateur a choisi un dialoue et la sauvegarde.
    if is_dialog and is_save:
        # Appel de la methode with_dialog, permettant le dialogue avec l'utilisateur.
        dialog.with_dialog()
        # Instantiation d'un objet SaveFile, permettant la sauvegarde des informations dans un fichier.
        save_file = SaveFile(data_to_save)
        # Appel de la methode save_with_path, permettant la sauvegarde a l'emplacement saisi par l'utilisateur.
        save_file.save_with_path()

    # Cas ou l'utilisateur n'a choisi que le dialogue.
    elif is_dialog and not(is_save):
        # Appel de la methode with_dialog, permettant le dialogue avec l'utilisateur.
        dialog.with_dialog()

    # Cas ou l'utilisateur n'a choisi que la sauvegarde.
    elif not(is_dialog) and is_save:
        # Affichage de toutes les informations.
        display.display_all()
        # Instantiation d'un objet SaveFile, permettant la sauvegarde des informations dans un fichier.
        save_file = SaveFile(data_to_save)
        # Appel de la methode simple_save, permettant la sauvegarde dans le repertoire courant.
        save_file.simple_save()

    # Cas ou l'utilisateur n'a choisi ni le dialogue ni la sauvegarde.
    else:
        # Affichage de toutes les informations.
        display.display_all()
