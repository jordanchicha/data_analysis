#
# data_recovery.py
# ProjetVTI
#
# Created by Jordan Chicha and Nessim Halfon on 03/10/2018.
# Copyright © 2018 Jordan Chicha and Nessim Halfon. All rights reserved
#

import pandas as pd
import magic
from datetime import datetime
from plot import Plot

class DataRecovery:
    """
    Cette classe permet le traitement des donnees contenue dans le fichier passe en parametre.
    """

    def __init__(self, path_file):
        """
        Constructeur de la classe.
        """

        # Permet d'initialiser les affichages des differentes DataFrame.
        # Permet de ne garder que 3 chiffres apres la virgule.
        pd.options.display.float_format = '{:.3f}'.format

        # Ignore l'affichage par defaut des DataFrame.
        # Ignore la taille du terminal en affichant toutes les donnees.
        pd.set_option('display.max_columns', None)
        pd.set_option('display.max_rows', None)

        # Le fichier contenant les donnees.
        self.file = path_file
        # La DataFrame contenant les donnees du fichier.
        self.df = pd.read_csv(self.file)
        # Permet d'ignorer la 1ere variable correspondant au nombre d'observation.
        self.df.drop(self.df.columns[0], axis=1, inplace=True)
        # La DataFrame qui contiendra les variables qualitatives.
        # Pour cela, on exclut toutes les type numerique.
        self.var_quali = self.df.select_dtypes(exclude=['float', 'int', 'long'])
        # La DataFrame qui contiendra les variables quantitatives.
        # Pour cela, on inclut toutes les type numerique.
        self.var_quanti = self.df.select_dtypes(include=['float', 'int', 'long'])

    def get_size(self):
        """
        Cette methode permet de retourner la taille du fichier en octets.
        """
        size = self.file.stat().st_size
        return size

    def get_encoding(self):
        """
        Cette methode permet de retourner l'encadage du fichier.
        """
        encoding = open(self.file).read()
        m = magic.Magic(mime_encoding=True)
        encoding = m.from_buffer(encoding)
        return encoding

    def get_time(self):
        """
        Cette methode permet de retourner la date de la derniere modification du fichier.
        """
        mtime = self.file.stat().st_mtime
        # Permet d'avoir un affichage plus lisible.
        mtime_datetime = datetime.fromtimestamp(mtime).strftime("%d/%m/%Y à %H:%M:%S")
        return mtime_datetime

    def get_var_number(self):
        """
        Cette methode permet de retourner le nombre de variables de la DataFrame.
        """
        return self.df.shape[1]

    def get_obs_number(self):
        """
        Cette methode permet de retourner le nombre d'observations de la DataFrame.
        """
        return self.df.shape[0]

    def get_var_quali_number(self):
        """
        Cette methode permet de retourner le nombre de variables qualitatives de la DataFrame.
        """
        return self.var_quali.shape[1]

    def get_var_quanti_number(self):
        """
        Cette methode permet de retourner le nombre de variables quantitatives de la DataFrame.
        """
        return self.var_quanti.shape[1]

    def get_var_names(self):
        """
        Cette methode permet de retourner les noms des variables de la DataFrame.
        """
        tabNames = []
        names = ''
        # Recuperation des differents noms dans un tableau.
        for value in self.df:
            tabNames.append(value)
        # Concatenation des differents noms contenus dans le tableau separes par une virgule.
        for i in range(len(tabNames)):
            if i+1 == len(tabNames):
                names += tabNames[i]
            else:
                names += tabNames[i]+", "
        return names

    def get_infos_quali(self):
        """
        Cette methode permet de retourner les informations des variables qualitatives de la DataFrame.
        Pour chaque variable la Frequence et l'Effectif.
        """
        quali =  []
        # Parcours des differentes variable qualitatives de la DataFrame.
        for value in self.var_quali:
            # Pour chaque variable calcul des Effectifs.
            headcount = self.df[value].value_counts()
            # Recuperation du nombre de modalites pour une variable.
            modality = headcount.index
            # Creation d'une DataFrame indexee par le nombre de modalites.
            df_quali = pd.DataFrame(index=modality)
            # Ajout des Effectifs dans la DataFrame.
            df_quali['Effectifs'] = headcount.values
            # Ajout des Frequences dans la DataFrame.
            df_quali['Fréquences %'] = df_quali['Effectifs'] / len(self.df) * 100
            # On creer une DataFrame clone de la DataFrame initiale pour pouvoir enlever la colonnes des frequences pour pouvoir l'afficher dans le plot.
            df_quali_for_plot = pd.DataFrame(df_quali)
            # On enleve la colonne des frequences.
            df_quali_for_plot.drop(df_quali_for_plot.columns[1], axis=1, inplace=True)
            # Creation d'un plot avec la matrice transposee.
            plot = Plot(df_quali_for_plot.transpose())
            # Sauvegarde du plot.
            plot.save_plot_quali(value)
            # Ajout dans un tableau les DataFrames des variables qualitatives.
            # Ajout dans un tableau les variables.
            quali.append([value, df_quali])
        return quali

    def get_infos_quanti(self):
        """
        Cette methode permet de retourner les informations des variables quantitatives de la DataFrame.
        Pour chaque variable le Minimum, le Maximum, la Moyenne, la Mediane et l'Ecart-Type.
        """
        quanti = []
        names = []
        mins = []
        maxs = []
        medians = []
        means = []
        stds = []

        # Parcours des differentes variable quantitatives de la DataFrame.
        for value in self.var_quanti:
            # Ajout des Noms dans un tableau.
            names.append(value)
            # Ajout des Minimums dans un tableau.
            mins.append(self.df[value].min())
            # Ajout des Maximums dans un tableau.
            maxs.append(self.df[value].max())
            # Ajout des Mediane dans un tableau.
            medians.append(self.df[value].median())
            # Ajout des Moyenne dans un tableau.
            means.append(self.df[value].mean())
            # Ajout des Ecarts-Types dans un tableau.
            stds.append(self.df[value].std())
        # Creation de la DataFrame en l'indexant avec les noms.
        df_quanti = pd.DataFrame(index=names)
        # Ajout des Minimums dans la DataFrame.
        df_quanti["Minimums"] = mins
        # Ajout des Maximums dans la DataFrame.
        df_quanti["Maximums"] = maxs
        # Ajout des Medianes dans la DataFrame.
        df_quanti["Médianes"] = medians
        # Ajout des Moyennes dans la DataFrame.
        df_quanti["Moyennes"] = means
        # Ajout des Ecarts-Types dans la DataFrame.
        df_quanti["Écarts-types"] = stds
        quanti.append(df_quanti)

        # Creation d'un plot.
        plot = Plot(df_quanti)
        # Sauvegarde du plot.
        plot.save_plot_quanti(names)

        return quanti
