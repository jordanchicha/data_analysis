Ce projet a pour objectif l'implementation d'un programme visant a la recuperation et au traitement des donnees contenues dans un fichier csv.

Tous les fichiers necessaires au code se trouve dans le repertoire src.

Veuillez commencer par telecharger Python3 sur votre machine si vous ne le possedez pas.

Lien de telechargement :

https://www.python.org/downloads/
(choisissez la version de Python 3 adapte a votre systeme d'exploitation)

Sur votre terminal rendez-vous dans le repertoire src et tapez la commande ci-après :

Telechargment des dependances necessaires a l'execution du programme:

- python3 -m pip install pandas
- python3 -m pip install python-magic
- python3 -m pip install libmagic

Si pip ne fonctionne pas, essayer de l'installer a l'aide de Homebrew comme suit:
  - brew install python-magic
  - brew install libmagic

- python3 -m pip install matplotlib

Executer ensuite le fichier main.py de la foncon suivante :

python3 main.py chemin_du_fichier dialog save
